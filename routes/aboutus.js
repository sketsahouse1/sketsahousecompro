var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');

let tabel_blogController = require('../controller/tabel_blogController.js');
router.get('/', function(req, res, next) {
	tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blogfooter = [];
        for (var i = 0; i < respon.length; i++) {
            if (i < 3) {
                blogfooter[i]={
                    _id:respon[i]._id,
                    image:respon[i].image,
                    title:respon[i].title,
                    description:respon[i].description.substr(0, 100)+'</p>',
                    time:dateFormat(respon[i].time, "fullDate")
                };  
            }
        }
        res.render('aboutus',{datafooter:blogfooter});
    });
});

module.exports = router;
