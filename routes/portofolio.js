var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');

/* GET users listing. */
let tabel_blogController = require('../controller/tabel_blogController.js');
let tabel_portofolioController = require('../controller/tabel_portofolioController.js');
router.get('/', function(req, res, next) {
	tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blogfooter = [];
        for (var i = 0; i < respon.length; i++) {
            if (i < 3) {
                blogfooter[i]={
                    _id:respon[i]._id,
                    image:respon[i].image,
                    title:respon[i].title,
                    description:respon[i].description.substr(0, 100)+'</p>',
                    time:dateFormat(respon[i].time, "fullDate")
                };  
            }
        }
        tabel_portofolioController.getTabelPortofolio(function(err,respon){
            if (err) {
                throw err;
            }
            res.render('portofolio',{datafooter:blogfooter,data:respon});
        })
    });
});

router.get('/:_id', function(req, res, next) {
    tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blogfooter = [];
        for (var i = 0; i < respon.length; i++) {
            if (i < 3) {
                blogfooter[i]={
                    _id:respon[i]._id,
                    image:respon[i].image,
                    title:respon[i].title,
                    description:respon[i].description.substr(0, 100)+'</p>',
                    time:dateFormat(respon[i].time, "fullDate")
                };  
            }
        }
        tabel_portofolioController.getTabelPortofolioById(req.params._id,function(err,respon){
            if(err){
                throw err;
            }
            console.log(respon);
            res.render('detailportofolio',{
                job_name:respon.job_name,
                client:respon.client,
                services:respon.services,
                image:respon.image,
                description:respon.description,
                datafooter:blogfooter
            });
        });
    });
});

module.exports = router;
