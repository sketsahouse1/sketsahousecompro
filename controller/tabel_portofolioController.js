tabel_portofolio = require('../model/tabel_portofolioModel.js');

module.exports.getTabelPortofolio = function(callback,limit){
    tabel_portofolio.find(callback).limit(limit);
}

module.exports.addTabelPortofolio = function(masukan,callback){
    tabel_portofolio.create(masukan,callback);
}

module.exports.removeTabelPortofolio = function(id,callback){
    tabel_portofolio.findByIdAndRemove(id,callback);
}

module.exports.getTabelPortofolioById = function(id,callback){
    tabel_portofolio.findById(id,callback);
}

module.exports.editTabelPortofolio = function(id,body,callback){
    tabel_portofolio.findByIdAndUpdate(id,body,callback);
}