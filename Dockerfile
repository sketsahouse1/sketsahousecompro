FROM mhart/alpine-node

RUN npm install pm2 -g

WORKDIR /src

COPY package.json .

RUN npm i

COPY . .

EXPOSE 80

EXPOSE 3000

CMD ["pm2-docker", "process.yml"]
