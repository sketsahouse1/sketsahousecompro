tabel_blog = require('../model/tabel_blogModel.js');

module.exports.getTabelBlog = function(callback,skip,limit){
    tabel_blog.find(callback).skip(skip).limit(limit);
}

module.exports.addTabelBlog = function(blog,callback){
    tabel_blog.create(blog,callback);
}

module.exports.removeTabelBlog = function(id,callback){
    tabel_blog.findByIdAndRemove(id,callback);
}

module.exports.getTabelBlogById = function(id,callback){
    tabel_blog.findById(id,callback);
}

module.exports.getTabelBlogByTitle = function(Title,callback){
    tabel_blog.find({title:Title},callback);
}

module.exports.getTabelBlogByImage = function(Image,callback){
    tabel_blog.find({image:Image},callback);
}

module.exports.editTabelBlog = function(id,body,callback){
    tabel_blog.findByIdAndUpdate(id,body,callback);
}