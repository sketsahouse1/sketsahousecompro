tabel_masukan = require('../model/tabel_masukanModel.js');

module.exports.getTabelMasukan = function(callback,limit){
    tabel_masukan.find(callback).limit(limit);
}

module.exports.addTabelMasukan = function(masukan,callback){
    tabel_masukan.create(masukan,callback);
}

module.exports.removeTabelMasukan = function(id,callback){
    tabel_masukan.findByIdAndRemove(id,callback);
}

module.exports.getTabelMasukanById = function(id,callback){
    tabel_masukan.findById(id,callback);
}

module.exports.editTabelMasukan = function(id,body,callback){
    tabel_masukan.findByIdAndUpdate(id,body,callback);
}