var express = require('express');
var router = express.Router();
var multer = require('multer');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/portofolio')
  },
  filename: function (req, file, cb) {
    cb(null, dateFormat(new Date(),'ddmmyyyyhms')+'.jpg')
  }
})
var dateFormat = require('dateformat');
var upload = multer({ storage : storage });
/* GET users listing. */
let tabel_portofolioController = require('../controller/tabel_portofolioController.js');
router.get('/', function(req, res, next) {
    jwt.verify(req.query.admin, 'sketsa', (err, decoded) => {
        if(decoded!=null && decoded.tgl==dateFormat(new Date(), "dd-mm-yyyy") &&
            decoded.jam==dateFormat(new Date(), "h") && decoded.nama=='sketsahouse' &&
            decoded.kode=='sketsahouse12345'){
            tabel_portofolioController.getTabelPortofolioLimit(function(err,respon){
                if(err){
                    throw err;
                }
                let portofolio=[];
                for (var i = 0; i < respon.length; i++) {
                    portofolio[i]={
                        _id:respon[i]._id,
                        job_name:respon[i].job_name,
                        client:respon[i].client,
                        services:respon[i].services,
                        image:respon[i].image,
                        description:respon[i].description,
                        admin:req.query.admin
                    }
                }
                res.render('portofolioadmin',{data:portofolio,admin:req.query.admin,pesan:req.query.pesan});
            });
        } else {
            res.render('error');
        }
    });
});

router.post('/add', upload.single('image'), function(req, res, next) {
    if(req.body){
        req.body.image = req.file.filename;
        tabel_portofolioController.addTabelPortofolio(req.body,function(err,respon){
            if(err){
                throw err;
            }
            res.redirect('/portofolioadmin?admin='+req.body.admin);
        });
    } else {
        res.render('error');
    }	
});

router.post('/hapus', function(req, res, next) {
    if (req.body) {
        tabel_portofolioController.removeTabelPortofolio(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            fs.unlink('public/uploads/portofolio/'+respon.image);
            res.redirect('/portofolioadmin?admin='+req.body.admin);
        });
    } else {
        res.render('error');
    }
});

module.exports = router;
