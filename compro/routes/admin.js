var express = require('express');
var router = express.Router();
var multer = require('multer');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var dateFormat = require('dateformat');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, dateFormat(new Date(),'ddmmyyyyhms')+'.jpg')
  }
})
var upload = multer({ storage : storage });
/* GET users listing. */
let tabel_blogController = require('../controller/tabel_blogController.js');
router.get('/', function(req, res, next) {
    jwt.verify(req.query.admin, 'sketsa', (err, decoded) => {
        console.log(decoded);
        if(decoded!=null && decoded.tgl==dateFormat(new Date(), "dd-mm-yyyy") &&
            decoded.jam==dateFormat(new Date(), "h") && decoded.nama=='sketsahouse' &&
            decoded.kode=='sketsahouse12345'){
            tabel_blogController.getTabelBlog(function(err,respon){
                if(err){
                    throw err;
                }
                let blog = [];
                for (var i = 0; i < respon.length; i++) {
                    blog[i]={
                        _id:respon[i]._id,
                        title:respon[i].title,
                        description:respon[i].description.substr(0, 150),
                        image:respon[i].image,
                        keyword:respon[i].keyword,
                        time:dateFormat(respon[i].time, "fullDate"),
                        admin:req.query.admin
                    };
                }
                res.render('admin',{data:blog,admin:req.query.admin,pesan:req.query.pesan});
            });
        } else {
            res.render('error');
        }
    });
});
router.post('/add', upload.single('image'), function(req, res, next) {
    if(req.body){
        tabel_blogController.getTabelBlogByTitle(req.body.title,function(err,respon){
            if(err){
                throw err;
            }
            if(respon[0]!=null){
                res.redirect('/admin?admin='+req.body.admin+'&pesan=Title Sudah Ada..!!!');
            } else {
                console.log(req.file);
                req.body.time = new Date();
                req.body.image = req.file.filename;
                tabel_blogController.addTabelBlog(req.body,function(err,respon){
                    if(err){
                        throw err;
                    }
                    res.redirect('/admin?admin='+req.body.admin);
                });
            }   
        });
    } else {
        res.render('error');
    }	
});

router.post('/hapus', function(req, res, next) {
    if (req.body) {
        tabel_blogController.removeTabelBlog(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            fs.unlink('public/uploads/'+respon.image);
            res.redirect('/admin?admin='+req.body.admin);
        });
    } else {
        res.render('error');
    }
});
router.post('/edit', function(req, res, next) {
    if (req.body) {
        tabel_blogController.getTabelBlogById(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            console.log(respon);
            res.render('editblog',{_id:respon._id,title:respon.title,image:respon.image,
                description:respon.description,keyword:respon.keyword,admin:req.body.admin});
        });
    } else {
        res.render('error');
    }
});
router.post('/edit/add', upload.single('image'),function(req, res, next) {
    if (req.body) {
        tabel_blogController.getTabelBlogByTitle(req.body.title,function(err,respon){
            if(err){
                throw err;
            }
            if (req.file) {
                req.body.image = req.file.filename;
                fs.unlink('public/uploads/'+req.body.gambar);
            }
            if(respon[0]!=null){
                if(respon[0]._id==req.body._id){
                    tabel_blogController.editTabelBlog(req.body._id,req.body,function(err,respon){
                        if(err){
                            throw err;
                        }
                        res.redirect('/admin?admin='+req.body.admin);
                    });
                } else {
                    res.redirect('/admin?admin='+req.body.admin+'&pesan=Title Sudah Ada..!!!');
                }
            } else {
                tabel_blogController.editTabelBlog(req.body._id,req.body,function(err,respon){
                    if(err){
                        throw err;
                    }
                    res.redirect('/admin?admin='+req.body.admin);
                });
            }
        });
    }else {
        res.render('error');
    }
});

module.exports = router;
