var express = require('express');
var router = express.Router();

/* GET users listing. */
var dateFormat = require('dateformat');
let tabel_blogController = require('../controller/tabel_blogController.js');
let tabel_masukanController = require('../controller/tabel_masukanController.js');
router.get('/', function(req, res, next) {
	tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blog = [];
        let tgl = "";
        for (var i = 0; i < 3; i++) {
            blog[i]={
                _id:respon[i]._id,
            	title:respon[i].title.substr(0, 30),
            	time:dateFormat(respon[i].time, "fullDate"),
            	image:respon[i].image
            };
        }
        res.render('contact',{data:blog});
    });
});

module.exports = router;
