var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var dateFormat = require('dateformat');
const nodemailer = require('nodemailer');
let tabel_masukanController = require('../controller/tabel_masukanController.js');
router.get('/', function(req, res, next) {
    jwt.verify(req.query.admin, 'sketsa', (err, decoded) => {
        if(decoded!=null && decoded.tgl==dateFormat(new Date(), "dd-mm-yyyy") &&
            decoded.jam==dateFormat(new Date(), "h") && decoded.nama=='sketsahouse' &&
            decoded.kode=='sketsahouse12345'){
            tabel_masukanController.getTabelMasukan(function(err,respon){
                if(err){
                    throw err;
                }
                let blog=[];
                for (var i=0; i < respon.length; i++) {
                    blog[i]={
                        _id:respon[i]._id,
                        name:respon[i].name,
                        email:respon[i].email,
                        subject:respon[i].subject,
                        comment:respon[i].comment,
                        time:dateFormat(respon[i].time,"fullDate"),
                        admin:req.query.admin,
                        reply:respon[i].reply
                    }
                }
                res.render('masukan',{data:blog,admin:req.query.admin,pesan:req.query.pesan});
            });
        } else {
            res.render('error');
        }
    });
});

router.post('/hapus', function(req, res, next) {
    if (req.body) {
        tabel_masukanController.removeTabelMasukan(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            res.redirect('/masukan?admin='+req.body.admin);
        });
    } else {
        res.render('error');
    }
});

router.post('/edit', function(req, res, next) {
    if (req.body) {
        tabel_masukanController.getTabelMasukanById(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            res.render('balasmasukan',{_id:respon._id,admin:req.body.admin,email:respon.email});
        });
    } else {
        res.render('error');
    }
});
router.post('/edit/add',function(req, res, next) {
    if (req.body) {
        console.log(req.body);
        var transporter = nodemailer.createTransport({
            service: 'Yandex',
            auth: {
                user: 'admin@sketsahouse.com',
                pass: 'sketsa123sketsa'
            }
        });

        var mailOptions = {
            from: 'admin@sketsahouse.com',
            to: req.body.email,
            subject: req.body.subject,
            text: req.body.message
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
        tabel_masukanController.getTabelMasukanById(req.body._id,function(err,respon){
            if(err){
                throw err;
            }
            let body = {
                name:respon.name,
                email:respon.email,
                subject:respon.subject,
                comment:respon.comment,
                time:respon.time,
                reply:""
            }
            tabel_blogController.editTabelBlog(req.body._id,body,function(err,respon){
                if(err){
                    throw err;
                }
                res.redirect('/masukan?admin='+req.body.admin);
            });
        });
    }else {
        res.render('error');
    }
});

module.exports = router;
