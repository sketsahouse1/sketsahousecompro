var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');

/* GET users listing. */
let tabel_blogController = require('../controller/tabel_blogController.js');
router.get('/', function(req, res, next) {
    if(req.query.skip != null && req.query.limit != null && req.query.skip >= 0 && req.query.limit >= 0){
    	tabel_blogController.getTabelBlog(function(err,respon){
            if(err){
                throw err;
            }
            let blog = [];
            for (var i = 0; i < respon.length; i++) {
                blog[i]={
                	_id:respon[i]._id,
                    image:respon[i].image,
                	title:respon[i].title,
                	description:respon[i].description.substr(0, 100)+'</p>',
                	time:dateFormat(respon[i].time, "fullDate")
                };
            }
            let prev = "";
            let next = "";
            let classnext = "active";
            let classprev = "active";
            if(req.query.skip>0){
                classprev = "";
                prev = "/blog?skip="+(req.query.skip-10)+"&limit="+(req.query.limit-10);
            }   
            if(respon.length>0){
                classnext = "";
                next = "/blog?skip="+(parseInt(req.query.skip)+10)+"&limit="+(parseInt(req.query.limit)+10);
            }
            res.render('blog',{data:blog,prev:prev,next:next,classprev:classprev,classnext:classnext});
        },parseInt(req.query.skip),parseInt(req.query.limit));
    } else {
        res.render('error');
    }
});

router.get('/:title', function(req, res, next) {
    tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blogfooter = [];
        for (var i = 0; i < respon.length; i++) {
            if (i < 3) {
                blogfooter[i]={
                    _id:respon[i]._id,
                    image:respon[i].image,
                    title:respon[i].title,
                    description:respon[i].description.substr(0, 100)+'</p>',
                    time:dateFormat(respon[i].time, "fullDate")
                };  
            }
        }
        tabel_blogController.getTabelBlogByTitle(req.params.title,function(err,respon){
            if(err){
                throw err;
            }
            res.render('detailblog',{title:respon[0].title,image:respon[0].image,
                description:respon[0].description,datafooter:blogfooter});
        });
    });
});

module.exports = router;
