var express = require('express');
var router = express.Router();
var dateFormat = require('dateformat');
let tabel_masukanController = require('../controller/tabel_masukanController.js');
let tabel_blogController = require('../controller/tabel_blogController.js');
let tabel_portofolioController = require('../controller/tabel_portofolioController.js');
const nodemailer = require('nodemailer');
router.get('/', function(req, res, next) {	
	tabel_blogController.getTabelBlog(function(err,respon){
        if(err){
            throw err;
        }
        let blog = [];
        let tgl = "";
        for (var i = 0; i < 4; i++) {
            blog[i]={
                _id:respon[i]._id,
                title:respon[i].title,
            	description:respon[i].description.substr(0, 100)+'</p>',
            	time:dateFormat(respon[i].time, "fullDate")
            };
        }
        tabel_portofolioController.getTabelPortofolio(function(err,respon){
            if(err){
                throw err;
            }
            let porto=[];
            for (var i = 0; i < 6; i++) {
                porto[i]={
                    _id:respon[i]._id,
                    job_name:respon[i].job_name,
                    client:respon[i].client,
                    services:respon[i].services,
                    image:respon[i].image,
                    description:respon[i].description
                }
            }
            res.render('index',{data:blog,portofolio:porto});
        });
    });
});

router.post('/add', function(req, res, next) {
	req.body.time = new Date();
    var transporter = nodemailer.createTransport({
      service: 'Yandex',
      auth: {
        user: 'admin@sketsahouse.com',
        pass: 'sketsa123sketsa'
      }
    });

    var mailOptions = {
      from: 'admin@sketsahouse.com',
      to: req.body.email,
      subject: req.body.subject,
      text: 'Terimakasih Telah Menghubungi Sketsa, Kami Segera Akan Menghubungi Anda Terkait Dengan Pesan Yang Anda Berikan.'
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
    req.body.reply = "hidden";
  	tabel_masukanController.addTabelMasukan(req.body,function(err,respon){
        if(err){
            throw err;
        }
    	res.redirect('/');    
    });
});

module.exports = router;
